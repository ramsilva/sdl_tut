#pragma once
#include <SDL.h>
#include "TextureWrapper.h"
#include <vector>

class Dot{
public:
	static const int DOT_WIDTH = 20;
	static const int DOT_HEIGHT = 20;

	static const int DOT_VEL = 10;

	Dot();
	~Dot();
	Dot(SDL_Renderer *ren, std::string spritePath);

	void handleEvent(SDL_Event &e);

	void move(std::vector<SDL_Rect> walls);
	void render(SDL_Renderer *ren);

private:
	Texture mSprite;
	int mPosX, mPosY;
	int mVelX, mVelY;

	SDL_Rect mBoundingBox;
};

