#pragma once
#include <SDL.h>
#include "TextureWrapper.h"
#include <SDL_ttf.h>
#include "Dot.h"

class SDLManager{
public:
	//Starts up SDL and creates window
	bool init();

	//Loads media
	bool loadMedia();

	//Frees media and shuts down SDL
	void close();

	//The window we'll be rendering to
	SDL_Window* gWindow = nullptr;

	//Current displayed image
	SDL_Surface* gCurrentSurface = nullptr;

	//The window renderer
	SDL_Renderer* gRenderer = nullptr;

	Dot *gDot = nullptr;
	std::vector<SDL_Rect> gWalls;
};