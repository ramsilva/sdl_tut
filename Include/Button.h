#pragma once
#include <SDL.h>
#include "TextureWrapper.h"

enum ButtonSprite{
	BUTTON_SPRITE_MOUSE_OUT,
	BUTTON_SPRITE_MOUSE_OVER_MOTION,
	BUTTON_SPRITE_MOUSE_DOWN,
	BUTTON_SPRITE_MOUSE_UP,
	BUTTON_SPRITE_TOTAL
};

class Button{
public:
	Button();

	void setPosition(int x, int y);
	void handleEvent(SDL_Event *e);
	void render(SDL_Renderer *ren);

	void setSprite(Texture *sprite);
	void setSpriteClips(SDL_Rect *clips);

private:
	SDL_Rect mSpriteClips[BUTTON_SPRITE_TOTAL];
	Texture mSprite;
	SDL_Point mPosition;
	ButtonSprite mCurrentSprite;
};