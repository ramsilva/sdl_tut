#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <SDL_ttf.h>

class Texture{
public:
	Texture();
	~Texture();

	//Loads image at specified path
	bool loadFromFile(SDL_Renderer *ren, std::string path);

#ifdef _SDL_TTF_H
	bool loadFromRenderedText(SDL_Renderer *ren, TTF_Font *font, std::string textureText, SDL_Color textColor);
#endif // _SDL_TTF_H

	//Deallocates texture
	void free();
	
	//Set color modulation
	void setColor(Uint8 red, Uint8 green, Uint8 blue);

	//Set blending
	void setBlendMode(SDL_BlendMode blendMode);

	//Set alpha modulation
	void setAlpha(Uint8 alpha);

	//Renders texture at given point
	void render(SDL_Renderer *ren, int x, int y, SDL_Rect * clip = nullptr, double angle = 0.0f, SDL_Point *center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Gets image dimensions
	int getWidth();
	int getHeight();

private:
	//The actual hardware texture
	SDL_Texture* mTexture;

	//Image dimensions
	int mWidth;
	int mHeight;
};

