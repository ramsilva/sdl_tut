#include "Dot.h"
#include "Constants.h"

Dot::Dot(){
	mPosX = 0;
	mPosY = 0;
	mVelX = mVelY = 0;
	mBoundingBox = {mPosX, mPosY, DOT_WIDTH, DOT_HEIGHT};
}

Dot::~Dot(){
	mSprite.free();
}

Dot::Dot(SDL_Renderer *ren, std::string spritePath) : Dot(){
	if (!mSprite.loadFromFile(ren, spritePath)){
		printf("Error loading dot sprite");
	}
}

void Dot::handleEvent(SDL_Event & e){
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0){
		switch (e.key.keysym.sym){
			case SDLK_LEFT: mVelX -= DOT_VEL; break;
			case SDLK_RIGHT: mVelX += DOT_VEL; break;
			case SDLK_UP: mVelY -= DOT_VEL; break;
			case SDLK_DOWN: mVelY += DOT_VEL; break;
		}
	} else if (e.type == SDL_KEYUP && e.key.repeat == 0){
		switch (e.key.keysym.sym){
			case SDLK_LEFT: mVelX += DOT_VEL; break;
			case SDLK_RIGHT: mVelX -= DOT_VEL; break;
			case SDLK_UP: mVelY += DOT_VEL; break;
			case SDLK_DOWN: mVelY -= DOT_VEL; break;
		}
	}
}

void Dot::move(std::vector<SDL_Rect> walls){
	SDL_Rect collision;
	
	mPosX += mVelX;
	mBoundingBox.x = mPosX;
	if ((mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH)){
		if (mVelX > 0){
			mPosX = SCREEN_WIDTH - DOT_WIDTH;
		} else if (mVelX < 0){
			mPosX = 0;
		}
	} else{
		for each (SDL_Rect wall in walls){
			//check collision with walls
			if (SDL_IntersectRect(&mBoundingBox, &wall, &collision)){
				if (mVelX > 0){
					mPosX = wall.x - DOT_WIDTH;
				} else if (mVelX < 0){
					mPosX = wall.x + wall.w;
				}
				break;
			}
		}
	}
	mBoundingBox.x = mPosX;

	mPosY += mVelY;
	mBoundingBox.y = mPosY;
	if ((mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT)){
		if (mVelY > 0){
			mPosY = SCREEN_HEIGHT - DOT_WIDTH;
		} else if (mVelY < 0){
			mPosY = 0;
		}
	} else{
		for each (SDL_Rect wall in walls){
			if (SDL_IntersectRect(&mBoundingBox, &wall, &collision)){
				if (mVelY > 0){
					mPosY = wall.y - DOT_HEIGHT;
				} else if (mVelY < 0){
					mPosY = wall.y + wall.h;
				}
				break;
			}
		}
	}
	mBoundingBox.y = mPosY;
}

void Dot::render(SDL_Renderer *ren){
	mSprite.render(ren, mPosX, mPosY);
}