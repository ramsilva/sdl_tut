#include "SDLManager.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include "Timer.h"
#include "Constants.h"

int main(int argc, char* args[]){
	SDLManager *manager = new SDLManager();

	//Start up SDL and create window
	if (!manager->init()){
		printf("Failed to initialize!\n");
		manager->close();
		return -1;
	}
	//Load media
	if (!manager->loadMedia()){
		printf("Failed to load media!\n");
		manager->close();
		return -1;
	}

	SDL_Event e;
	bool quit = false;

	while (!quit){
		while (SDL_PollEvent(&e)){
			switch (e.type){
				case SDL_QUIT:
					quit = true;
					break;
				default:
					manager->gDot->handleEvent(e);
					break;
			}
		}
		SDL_SetRenderDrawColor(manager->gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderClear(manager->gRenderer);

		manager->gDot->move(manager->gWalls);

		SDL_SetRenderDrawColor(manager->gRenderer, 0x00, 0x00, 0x00, 0xFF);
		SDL_RenderDrawRects(manager->gRenderer, &manager->gWalls[0], 2);
		
		manager->gDot->render(manager->gRenderer);
		SDL_RenderPresent(manager->gRenderer);		
	}

	//Free resources and close SDL
	manager->close();

	return 0;
}