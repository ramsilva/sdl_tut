#include "Button.h"
#include "SDLManager.h"
#include "Helper.h"
#include "Constants.h"

Button::Button(){
	mPosition = {0, 0};
	mCurrentSprite = BUTTON_SPRITE_MOUSE_OUT;
}

void Button::setPosition(int x, int y){
	mPosition = {x, y};
}

void Button::handleEvent(SDL_Event *e){
	if (e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP || e->type == SDL_MOUSEMOTION){
		int x, y;
		SDL_GetMouseState(&x, &y);

		bool inside = true;
		if (x < mPosition.x){
			inside = false;
		} else if (x > mPosition.x + BUTTON_WIDTH){
			inside = false;
		} else if (y < mPosition.y){
			inside = false;
		} else if (y > mPosition.y + BUTTON_HEIGHT){
			inside = false;
		}

		if (!inside){
			mCurrentSprite = BUTTON_SPRITE_MOUSE_OUT;
		} else{
			switch (e->type){
				case SDL_MOUSEMOTION:
					mCurrentSprite = BUTTON_SPRITE_MOUSE_OVER_MOTION;
					break;
				case SDL_MOUSEBUTTONDOWN:
					mCurrentSprite = BUTTON_SPRITE_MOUSE_DOWN;
					break;
				case SDL_MOUSEBUTTONUP:
					mCurrentSprite = BUTTON_SPRITE_MOUSE_UP;
					break;
				default:
					break;
			}
		}
	}
}

void Button::render(SDL_Renderer *ren){
	mSprite.render(ren, mPosition.x, mPosition.y, &mSpriteClips[mCurrentSprite]);
}

void Button::setSprite(Texture *sprite){
	mSprite = *sprite;
}

void Button::setSpriteClips(SDL_Rect *clips){
	for (int i = 0; i < sizeof(mSpriteClips)/sizeof(*mSpriteClips); i++){
		mSpriteClips[i] = clips[i];
	}
}
