/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL and standard IO

#include "SDLManager.h"
#include <SDL_image.h>
#include <string>
#include "res_path.h"
#include "Constants.h"

bool SDLManager::init(){
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0){
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	} else{
		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == nullptr){
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		} else{
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == nullptr){
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags)) & imgFlags){
				success = false;
			}

			if (TTF_Init() == -1){
				printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
				success = false;
			}
		}
	}

	return success;
}

bool SDLManager::loadMedia(){
	//Loading success flag
	bool success = true;

	gDot = new Dot(gRenderer, "dot.bmp");
	SDL_Rect wall1 = {300, 40, 40, 400};
	SDL_Rect wall2 = {30, 400, 400, 40};
	gWalls = std::vector<SDL_Rect>(0);
	gWalls.insert(gWalls.begin(), wall1);
	gWalls.insert(gWalls.begin(), wall2);

	return success;
}

void SDLManager::close(){
	delete gDot;

	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);

	gRenderer = nullptr;
	gWindow = nullptr;

	//Quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}