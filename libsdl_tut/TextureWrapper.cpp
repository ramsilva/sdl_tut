#include "TextureWrapper.h"
#include "res_path.h"

Texture::Texture(){
	mTexture = nullptr;
	mWidth = 0;
	mHeight = 0;
}

Texture::~Texture(){ free(); }

bool Texture::loadFromFile(SDL_Renderer *ren, std::string path){
	free();
	
	SDL_Texture *newTexture = nullptr;
	SDL_Surface *loadedSurface = IMG_Load((getResourcePath() + path).c_str());
	if (!loadedSurface){
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
		return false;
	}

	SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0x00, 0xFF, 0xFF));
	
	newTexture = SDL_CreateTextureFromSurface(ren, loadedSurface);
	if (!newTexture){
		printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		return false;
	}

	mWidth = loadedSurface->w;
	mHeight = loadedSurface->h;

	SDL_FreeSurface(loadedSurface);
	
	mTexture = newTexture;
	return true;
}

#ifdef _SDL_TTF_H
bool Texture::loadFromRenderedText(SDL_Renderer * ren, TTF_Font *font, std::string textureText, SDL_Color textColor){
	free();

	SDL_Surface *textSurface = TTF_RenderText_Solid(font, textureText.c_str(), textColor);
	if (!textSurface){
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}

	mTexture = SDL_CreateTextureFromSurface(ren, textSurface);
	if (!mTexture){
		printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		return false;
	}

	mWidth = textSurface->w;
	mHeight = textSurface->h;

	SDL_FreeSurface(textSurface);
	
	return true;
}
#endif

void Texture::free(){
	if (mTexture){
		SDL_DestroyTexture(mTexture);
		mTexture = nullptr;
		mWidth = 0;
		mHeight = 0;
	}
}

void Texture::setColor(Uint8 red, Uint8 green, Uint8 blue){
	SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void Texture::setBlendMode(SDL_BlendMode blendMode){
	SDL_SetTextureBlendMode(mTexture, blendMode);
}

void Texture::setAlpha(Uint8 alpha){
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

void Texture::render(SDL_Renderer * ren, int x, int y, SDL_Rect *clip, double angle, SDL_Point *center, SDL_RendererFlip flip){
	SDL_Rect renderQuad = {x, y, mWidth, mHeight};

	if (clip){
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopyEx(ren, mTexture, clip, &renderQuad, angle, center, flip);
}

int Texture::getWidth(){
	return mWidth;
}

int Texture::getHeight(){
	return mHeight;
}
